﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverDash : MonoBehaviour
{
	[SerializeField]
	float maxSpeed = 0.5f;

	[SerializeField]
	float accel = 0.01f;

	float vx = 0;
	Transform trans = null;

	void Awake()
	{
		trans = transform;
	}

	void Update()
	{
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			vx -= accel;
		}
		else if(Input.GetKey(KeyCode.RightArrow))
		{
			vx += accel;
		}
		else
		{
			vx -= accel * 0.5f;
		}

		if(vx < 0 )
		{
			vx = 0;
		}

		if(maxSpeed < vx)
		{
			vx = maxSpeed;
		}

		trans.position = new Vector3(trans.position.x + vx, trans.position.y, trans.position.z);
	}
}
